import  express, { Request, Response }  from "express";
import {body} from 'express-validator';
import jwt  from "jsonwebtoken";
import { User } from "../models/user";
import {validateRequest,BadRequestError} from '@emnaticketes/common'
import { Password } from "../services/password";
const router =express.Router();

router.post('/api/users/signin',[
   body('email').isEmail().withMessage('Email must be valid'),
    // trim vérifier no espace
    body('password').trim().notEmpty().withMessage('You must suppy a password')
],validateRequest,async (req:Request,res:Response)=>{

   const { email, password } = req.body;

   const existingUser = await User.findOne({ email });

   if (!existingUser) {
     throw new BadRequestError('Invalid credentials');
   }

   const passwordsMatch = await Password.compare(
     existingUser.password,
     password
   );

   if (!passwordsMatch) {
     throw new BadRequestError('Invalid Credentials');
   }

   // Generate JWT si il connecte
   const userJwt = jwt.sign(
     {
       id: existingUser.id,
       email: existingUser.email
     },
     process.env.JWT_KEY!
     // le clé
   );

   // Store it on session object
   req.session = {
     jwt: userJwt
   };

   res.status(200).send(existingUser);
});

export {router as signinRouter} ;
