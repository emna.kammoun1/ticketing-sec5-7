import {MongoMemoryServer} from 'mongodb-memory-server';
import mongoose from 'mongoose';
import request from 'supertest' ;
import { app } from '../app';

declare global {
  namespace NodeJS {
    export interface Global {
      signin(): Promise<string[]>;
    }
  }
}

let mongo:any ;

//demarrer un serveur de memoir MongoMemoryServer et laisser mongoose connect to it : instance of MongoMemoryServer
beforeAll(async ()=>{
    process.env.JWT_KEY = 'asdfasdf'
    mongo = await MongoMemoryServer.create();
    const mongoUri = mongo.getUri();
    await mongoose.connect(mongoUri, {});
});

// supprimer les collections dans mongoose ou réinitialiser
beforeEach( async ()=>{
    const collections = await mongoose.connection.db.collections();
    for (let collection of collections) {
        await collection.deleteMany({});
    }
});

// after finish all test stop MongoMemoryServer et tell mongoose to disconnect 
afterAll(async () => {
    if (mongo) {
      await mongo.stop();
    }
    await mongoose.connection.close();
  });
  global.signin =async () => {
    const email = 'test@test.com' ;
    const password = 'password' ;

    const response = await request(app)
    .post('/api/users/signup')
    .send({
      email: 'test@test.com',
      password: 'password'
    })
    .expect(201);

    const cookie =response.get('Set-Cookie');

    return cookie;
}
