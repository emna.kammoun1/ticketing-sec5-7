import mongoose from "mongoose";
import { Password } from "../services/password"; 

 // An interface that describes  the properties 
 // that are requires to create a new User

 //une interface qui décrit les propriétés requises pour créer un nouvel utilisateur.
interface UserAttrs{
    email: string ;
    password: string;
}
// An interface that describes  the properties
// that a User Model has


// Déclare une interface qui décrit les propriétés d'un modèle d'utilisateur.
interface UserModel extends mongoose.Model <UserDoc>{
   build(attrs:UserAttrs):UserDoc ;
}
// An interface that describes  the properties that a User User Document has
// Déclare une interface qui décrit les propriétés d'un document utilisateur.
interface UserDoc extends mongoose.Document {
    email: string ;
    password: string;
}
const userSchema = new mongoose.Schema({
    email:{
        type : String,
        required: true
    },
    password : {
        type: String ,
        required : true
    } },
    {
        toJSON:{
            transform(doc,ret){
                ret.id = ret._id;
                delete ret._id ;
               delete ret.password;
               delete ret.__v; 
            }
        }
    }
    );
    

//save a document in mongodb

//Avant de sauvegarder un document utilisateur, ce hook s'exécute.
userSchema.pre('save', async function (done){
    //Il vérifie si le mot de passe a été modifié.
    // Si le mot de passe a été modifié, il utilise la méthode Password.toHash pour hacher le mot de passe et le stocke à la place.

   if (this.isModified('password')){
     const hashed = await Password.toHash(this.get('password'));
     this.set('password',hashed);
    } 

    //indique que le hook a terminé son exécution.
    done();
})


// Crée une méthode statique build qui prend des attributs de type UserAttrs et renvoie une nouvelle instance de modèle d'utilisateur.
userSchema.statics.build = (attrs:UserAttrs) =>{
    return new User(attrs);
};

const User = mongoose.model<UserDoc,UserModel>('User',userSchema);

export {User} ;

